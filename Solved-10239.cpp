#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
using namespace std;
int abs(int x){return x>0?x:-x; }

int main()
{
    ios::sync_with_stdio(false);
    int t, m, n, j, k;
    int minDiff;
    cin >> t;
    for(int i=0; i<t; i=i+1)
    {
        minDiff=10000000;
        cin >> n;
        vector<int> alt1(n, 0);
        for(j=0; j<n ;j=j+1) {cin >> alt1[j];}
        cin >> m;
        vector<int> alt2(m, 0);
        for(j=0; j<m; j=j+1) {cin >> alt2[j];}
        for(k=0; k<n; k=k+1)
        {
            for(j=0; j<m; j=j+1)
            {
                if(abs(alt1[k]-alt2[j])<minDiff)
                    minDiff = abs(alt1[k]-alt2[j]);
            }
        }
        cout << minDiff << endl;
    }

    return 0;
}
