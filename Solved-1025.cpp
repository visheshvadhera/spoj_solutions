#include<iostream>
#include<algorithm>

using namespace std;

void hotness(int participants)
{
    int men[participants];
    int women[participants];
    int hotness_bonds=0;

    for(int k=0; k<participants; k=k+1)
        cin >> men[k];

    for(int l=0 ;l<participants; l=l+1)
        cin >> women[l];

    sort(men, men+participants);
    sort(women, women+participants);

    for(int j=0; j<participants; j=j+1)
        hotness_bonds += men[j]*women[j];

    cout << hotness_bonds << endl;

}

int main()
{
    ios::sync_with_stdio(false);
    int test_cases;
    cin >> test_cases;
    int no_of_participants;

    for(int i=0; i<test_cases; i=i+1)
    {
        cin >> no_of_participants;
        hotness(no_of_participants);

    }
    return 0;
}
