#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
using namespace std;

long long ans(long long k)
{
    int rem = (k-1)%4;
    long long q=(k-1)/4;
    if(rem==0)
        return 1000*q+192;
    else if(rem==1)
        return 1000*q+442;
    else if(rem==2)
        return 1000*q+692;
    else
        return 1000*q+942;
}

int main()
{
    ios::sync_with_stdio(false);
    long long t;
    long long k;
    cin >> t;
    for(int i=0; i<t; i=i+1)
    {
        cin >> k;
        cout << ans(k) << endl;
    }

    return 0;
}
