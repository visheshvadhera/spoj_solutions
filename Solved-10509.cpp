
#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<algorithm>
#include<stack>
#include<iomanip>
#include<cmath>
#include<map>

using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    int t, j;
    long long sum, l;
    cin >> t;
    for(j=0;j<t;j=j+1)
    {
        cin >> l;
        sum = (l*(3*l+1)/2);
        cout << sum%1000007 << endl;
    }

    return 0;
}
