#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<deque>


using namespace std;

void printMax(vector<int> arr, int n, int k)
{
    deque<int> Qi(k);
    int i;
    for(i=0; i<k; i=i+1)
    {
        while(!Qi.empty() && arr[i]>=arr[Qi.back()])
            Qi.pop_back();
        Qi.push_back(i);
    }
    for(;i<n; i=i+1)
    {
        cout << arr[Qi.front()] << " ";

        while(!Qi.empty() && Qi.front() <=i-k)
            Qi.pop_front();

        while ( (!Qi.empty()) && arr[i] >= arr[Qi.back()])
            Qi.pop_back();

        Qi.push_back(i);
    }
    cout << arr[Qi.front()];
}

int main()
{
    ios::sync_with_stdio(false);
    int nElements;
    cin >> nElements;
    vector<int> elements(nElements, 0);
    for(int i=0; i<nElements; i++)
    {
        cin >> elements[i];
    }
    int window;
    cin >> window;
    printMax(elements, nElements, window);

    return 0;
}
