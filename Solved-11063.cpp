#include<iostream>

using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    int test;
    cin >> test;
    long long thrd, last_thrd, sum, terms, diff, first;
    for(int i=0; i<test; i=i+1)
    {
        cin >> thrd >> last_thrd >> sum;
        terms = (2*sum)/(thrd+last_thrd);
        cout << terms << endl;
        diff = (last_thrd-thrd)/(terms-5);
        first = thrd - 2*diff;
        for(long long j=1; j<=terms; j=j+1)
            cout << first + (j-1)*diff << " ";
    }
    return 0;
}
