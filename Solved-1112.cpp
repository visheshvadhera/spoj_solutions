#include<iostream>

using namespace std;

void find_no(int x, int y)
{
    if(y-x==0)
    {
        if(x%2!=0)
            cout << 2*x-1 << endl;
        else
            cout << 2*x << endl;
    }
    else if(x-y==2)
    {
        if(x%2==0)
            cout << 2*x-2 << endl;
        else
            cout << 2*x-3 << endl;
    }
    else
        cout << "No Number" << endl;
}

int main()
{
    ios::sync_with_stdio(false);
    int cases;
    int x, y;
    cin >> cases;

    for(int i=0; i<cases; i=i+1)
    {
        cin >> x >> y;
        find_no(x, y);
    }
    return 0;
}
