#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<algorithm>
#include<stack>
#include<iomanip>
#include<cmath>
#include<map>

using namespace std;
int main()
{
    ios::sync_with_stdio(false);
    long long d_no, q;
    cin >> d_no;
    if(d_no%10==0)
    {
        cout << 2 << endl;
    }
    else
    {
        cout << 1 << endl;
        cout << d_no%10 << endl;
    }


    return 0;
}
