#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<algorithm>
#include<stack>

using namespace std;

const int N = 10002;
vector<int> graph[N];
bool mark[N] = {false};
bool ans;

void DFS(int x)
{
    int i, c;
    c = 0;

    for(i=0; i<graph[x].size(); i=i+1)
        if(mark[graph[x][i]]==true)
            c=c+1;

    if(c>1)
        ans = false;

    for(i=0; i<graph[x].size(); i=i+1)
    {
        if(mark[graph[x][i]]==false)
        {
            mark[graph[x][i]] = true;
            DFS(graph[x][i]);
        }
    }
}

int main()
{
    ios::sync_with_stdio(false);
    int j, n, e, n1, n2;
    ans = true;
    cin >> n >> e;
    for(int i=0; i<e; i=i+1)
    {
        cin >> n1 >> n2;
        graph[n1].push_back(n2);
        graph[n2].push_back(n1);
    }
    if(n!=e+1)
        ans = false;

    mark[1] = true;
    DFS(1);

    for(j=1; j<=n; j=j+1)
    {
        if(!mark[j])
        {
            ans = false;
            break;
        }
    }
    if(ans==false)
        cout << "NO" ;
    else
        cout << "YES" ;

    return 0;
}
