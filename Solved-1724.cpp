#include<iostream>
#include<string>
#include<sstream>
#include<vector>

using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    unsigned long long ans, n, t;
    cin >> t;
    for(int i=0; i<t; i=i+1)
    {
        cin >> n;
        if(n%2==0)
            ans = (n*(n+2)*(2*n+1))/8;
        else
            ans = (n*(n+2)*(2*n+1)-1)/8;
        cout << ans << endl;
    }

    return 0;
}
