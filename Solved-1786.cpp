#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
using namespace std;
#define MAX 99000001
int last[MAX];

int odd(int i) { return 2*i-1;}

int decode(string s)
{
    int x, y, z;
    x = s[0] - '0';
    y = s[1] - '0';
    z = s[3] - '0';
    return (10*x + y)*pow(10, z);
}

void preprocess()
{
    int i, hi, lo, countOfOdd, j, oddN, counter;
    memset(last, 0, sizeof(last));
    hi=1;
    for(i=1; i<=26; i=i+1)
    {
        lo = hi; hi = pow(2, i);
        countOfOdd = hi-lo;
        counter = 0;
        while(lo+counter<hi)
        {
            last[lo+counter] = odd(counter+1);
            counter+=1;
        }
    }
    lo=hi; hi=MAX; counter=0;
    while(lo+counter<hi)
    {
        last[lo+counter] = odd(counter+1);
        counter+=1;
    }
}

int main()
{
    ios::sync_with_stdio(false);
    int n=0;
    preprocess();
    while(true)
    {
        string s;
        cin >> s;
        if(s[0]=='0' && s[1]=='0' && s[3]=='0')
            break;

        n = decode(s);
        cout << last[n] << endl;
    }


    return 0;
}
