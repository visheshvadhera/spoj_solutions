#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>

using namespace std;

bool prime[10000];
bool visited[10000];
int layer[10000];

int rplc(int current, int pos, int digit)
{
    int q = 1000;
    int rem, result;
    if(pos==0)
    {
        rem = current%1000;
        result = q*digit + rem;
        return result;
    }
    else if(pos==1)
    {
        result=(current/q)*q;
        current = current%q;
        q = q/10;
        rem = current%q;
        result = result + q*digit + rem;
        return result;
    }
    else if(pos==2)
    {
        result=(current/q)*q;
        current = current%q;
        q = q/10;
        result = result + (current/q)*q;
        current = current%q;
        q=q/10;
        rem = current%q;
        result = result + q*digit + rem;
        return result;
    }
    else
    {
        result=(current/q)*q;
        current = current%q;
        q = q/10;
        result = result + (current/q)*q;
        current = current%q;
        q=q/10;
        result = result + (current/q)*q + digit;
        return result;
    }
}

void sieve()
{
    memset(prime, true, sizeof(prime));
    prime[0] = false;
    prime[1] = false;
    int i;
    for(i=2; i<10000; i=i+1)
    {
        if(prime[i])
        {
            int j=2;
            while(j*i<10000)
            {
                prime[j*i] = false;
                j=j+1;
            }
        }
    }

}


int bfs(int a, int b)
{
    memset(visited, false, sizeof(visited));
    memset(layer, -1, sizeof(layer));
    queue<int> q;
    int current, i, j, tmp;
    q.push(a);
    visited[a] = true;
    layer[a] = 0;
    while(!q.empty())
    {
        current = q.front();
        q.pop();
        for(i=0; i<4; i=i+1)
        {
            for(j=0; j<=9; j=j+1)
            {
                if(j==0&&i==0)
                    continue;
                else
                {
                tmp = rplc(current, i, j);
                if(!visited[tmp]&&prime[tmp]&&tmp!=current)
                {
                    visited[tmp] = true;
                    layer[tmp] = 1 + layer[current];
                    q.push(tmp);
                    if(tmp==b)
                       return layer[tmp];
                }
                }
            }
        }
    }
    return layer[b];
}

int main()
{
    ios::sync_with_stdio(false);
    int t, a, b;
    sieve();
    cin >> t;
    for(int i=0; i<t; i=i+1)
    {
        cin >> a >> b;
        cout << bfs(a, b) << endl;
    }
    return 0;
}
