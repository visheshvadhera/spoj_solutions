#include<iostream>

using namespace std;

void resolve(int packets)
{
    int candies[packets];
    int number;
    int mean = 0;
    int moves=0;

    for(int i=0; i<packets; i=i+1)
    {
        cin >> number;

        candies[i] = number;

        mean = mean+number;
    }

    if(mean%packets==0)
    {
        mean=mean/packets;
        for(int j=0; j<packets; j=j+1)
        {
            if(candies[j]>mean)
                moves = moves+ (candies[j]-mean);
        }
        cout << moves << endl;
    }
    else
        cout << -1 << endl;


}

int main()
{
    ios::sync_with_stdio(false);
    int no_of_packets;
    do
    {
        cin >> no_of_packets;
        if(no_of_packets==-1)
            continue;
        resolve(no_of_packets);
    }
    while(no_of_packets!=-1);
    return 0;

}
