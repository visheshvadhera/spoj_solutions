#include<iostream>

using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    long long int test;
    cin >> test;
    long long int total, x;
    int j, children;
    for(int i=0; i<test; i=i+1)
    {
        total = 0;
        cout << endl;
        cin >> children;
        for(j=0; j<children; j=j+1)
        {
            cin >> x;
            total=total+x;
            if(total>=children)
                total = total%children;
        }
        if(total%children==0)
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
    }

    return 0;
}
