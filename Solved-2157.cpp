#include<iostream>
#include<string>
#include<sstream>

using namespace std;

int strToInt(string &s)
{
    int b=0, ten=1;
    for(int i=s.size()-1; i>=0; i=i-1)
    {
        b = b + (s[i]- '0')*ten;
        ten = ten*10;
    }
    return b;
}

bool ifNumber(string &s)
{
    for(int i=0; i<s.size(); i=i+1)
    {
        if(s[i]>'9' || s[i]<'0')
            return false;
    }
    return true;
}

void solver(string s1, string s2, string s3)
{
     if(!ifNumber(s1))
    {
        cout << strToInt(s3) - strToInt(s2) << " + " << s2 << " = " << s3 <<endl;
    }
    else if(!ifNumber(s2))
    {
        cout << s1 << " + " << strToInt(s3) - strToInt(s1) << " = " << s3 << endl;
    }
    else
    {
        cout << s1 << " + " << s2 << " = " << strToInt(s1) +  strToInt(s2) << endl;
    }
}


int main()
{
    ios::sync_with_stdio(false);
    int t;
    cin >> t;
    string s1, s2, s3, s4, s5;

    for(int k=0; k<t; k=k+1)
    {
        cin >> s1 >> s2 >> s3 >> s4 >> s5;
        solver(s1, s3, s5);
    }
    return 0;
}
