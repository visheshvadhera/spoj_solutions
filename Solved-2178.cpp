#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<algorithm>
#include<stack>
#include<iomanip>
#include<cmath>
#include<map>

using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    int A, D, i;
    while(true)
    {
        cin >> A >> D;
        if(A==0)
            break;
        int att[A];
        int def[D];
        for(i=0; i<A;i=i+1)
            cin >> att[i];
        for(i=0; i<D;i=i+1)
            cin >> def[i];
        sort(att, att+A);
        sort(def, def+D);

        if(att[0]<def[1])
            cout << "Y" << endl;
        else
            cout << "N" << endl;

    }
    return 0;
}
