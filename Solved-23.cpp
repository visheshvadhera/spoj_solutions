#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
#include<iomanip>


using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    int t;
    double U, V, W, u, v, w;
    double X, Y, Z, x, y, z;
    double a, b, c, d;
    double vol;
    cin >> t;
    for(int i=0; i<t; i=i+1)
    {
        cin >> U >> W >> v >> V >> w >> u;
        X = (w-U+v)*(U+v+w); Y = (u-V+w)*(u+V+w); Z = (u-W+v)*(u+W+v);
        x = (U-v+w)*(v-w+U); y = (V-w+u)*(w-u+V); z = (W-u+v)*(u-v+W);
        a = sqrt(x*Y*Z); b = sqrt(y*Z*X); c = sqrt(z*X*Y); d = sqrt(x*y*z);
        vol = sqrt((-a+b+c+d)*(a-b+c+d)*(a+b-c+d)*(a+b+c-d))/(192*u*v*w);
        cout << fixed;
        cout << setprecision(4) << vol << endl;
    }
    return 0;
}
