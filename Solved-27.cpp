#include<iostream>
#include<map>

using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    int t, na;
    string blank;
    cin >> t;
    for(int i=0; i<t; i=i+1)
    {
        cin >> na;
        map<string, int> acc;
        for(int j=0; j<na; j=j+1)
        {
            string accNo("");
            string temp;
            for(int k=0; k<6;k=k+1)
            {
                cin >> temp;
                accNo.append(temp);
                accNo.append(" ");
            }
            if(acc.find(accNo)==acc.end())
                acc[accNo] = 1;
            else
                acc[accNo] +=1;
        }
        map<string, int>::iterator it;
        for(it=acc.begin(); it!=acc.end(); ++it)
            cout << (*it).first << " " << (*it).second << endl;
        getline(cin, blank);
    }
}
