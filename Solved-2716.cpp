#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<algorithm>
#include<stack>
#include<iomanip>
#include<cmath>
#include<map>

using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    int t;
    cin >> t;
    double a, b, c, d;
    double s, ar, area;
    for(int i=0; i<t;i=i+1)
    {
        cin >> a >> b >> c >> d;
        s = (a+b+c+d)/2;
        area = (s-a)*(s-b)*(s-c)*(s-d);
        ar = sqrt(area);
        cout << fixed;
        cout << setprecision(2) << ar << endl;
    }

    return 0;
}
