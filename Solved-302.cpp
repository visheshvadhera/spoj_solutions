#include<iostream>
#include<string>
#include<sstream>
#include<vector>

#define MAX 3163
int num[MAX][MAX] = {0};
int den[MAX][MAX] = {0};
int tri[4472] = {0};

using namespace std;

void cantor(int n)
{
    int k = 0;
    int nxt, prev, pindex, nindex, diff;
    while(n>=tri[k])
    {
        k=k+1;
    }
    prev = tri[k-1];
    pindex = k-1;
    if(n==prev)
    {
        if(pindex%2==0)
            cout << "TERM " << n << " IS " << num[0][pindex] << "/" << den[0][pindex] << endl;
        else
            cout << "TERM " << n << " IS " << num[pindex][0] << "/" << den[pindex][0] << endl;
    }
    else if(n==prev+1)
    {
        if(pindex%2==0)
            cout << "TERM " << n << " IS " << num[0][pindex+1] << "/" << den[0][pindex+1] << endl;
        else
            cout << "TERM " << n << " IS " << num[pindex+1][0] << "/" << den[pindex+1][0] << endl;

    }
    else
    {
        diff = n-(prev+1);
        if(pindex%2==0)
            cout << "TERM " << n << " IS " << num[diff][pindex+1-diff] << "/" << den[diff][pindex+1-diff] << endl;
        else
            cout << "TERM " << n << " IS " << num[pindex+1-diff][diff] << "/" << den[pindex+1-diff][diff] << endl;
    }
}

int main()
{
    ios::sync_with_stdio(false);
    int i, j, t;
    for(i = 0; i<3163; i=i+1)
    {
        for(j=0; j<3163; j=j+1)
        {
            num[i][j] = i+1;
            den[i][j] = j+1;
        }
    }
    for(i=0; i<4472; i=i+1)
    {
        tri[i] = ((i+1)*(i+2))/2;
    }

    cin >> t;
    int in;

    for(i=0; i<t; i=i+1)
    {
        cin >> in;
        cantor(in);
    }

    return 0;
}
