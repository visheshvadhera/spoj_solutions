#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<algorithm>
#include<stack>
#include<iomanip>
#include<cmath>
#include<map>

using namespace std;

void solver(string s)
{
    vector<int> a, ans;
    int i, j=1, p;
    for(i=s.size()-1; i>=0; i=i-1)
    {
        a.push_back(s[i]-'0');
    }

    int carry=0, quo, rem;
    for(i=0; i<s.size(); i=i+1)
    {
        rem = (a[i]*2 + carry)%10;
        quo = (a[i]*2 + carry)/10;
        ans.push_back(rem);
        carry = quo;
    }

    if(carry!=0)
        ans.push_back(carry);

    ans[0] = ans[0]-2;

    if(ans[0]<0)
    {
        ans[0]=ans[0]+10;
        while(ans[j]==0)
        {
            ans[j] = 9;
            j=j+1;
        }
        ans[j] = ans[j] - 1;
    }
    if(ans[ans.size()-1]==0)
        ans.pop_back();
    for(i=ans.size()-1; i>=0; i=i-1)
        cout << ans[i];

    cout << endl;
}

int main()
{
    ios::sync_with_stdio(false);
    string boardSize;

    while(cin>>boardSize)
    {
        if(boardSize=="0")
        {
            cout << 0 << endl;
            continue;
        }
        if(boardSize=="1")
        {
            cout << 1 << endl;
            continue;
        }
        solver(boardSize);
    }



    return 0;
}



