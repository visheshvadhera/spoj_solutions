#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<algorithm>
#include<numeric>

using namespace std;

bool myf(int l, int m) {return l>m;}

int main()
{
    ios::sync_with_stdio(false);
    int sum, j, stmps, frnds, d, f, s;
    vector<int> st;
    cin >> s;
    for(int i=1; i<=s; i=i+1)
    {
        cin >> stmps >> frnds;
        for(j=0; j<frnds;j=j+1)
        {
            cin >> d;
            st.push_back(d);
        }
        long long total = accumulate(st.begin(), st.end(), 0);
        if(total<stmps)
            cout << "Scenario #" << i << ":" << endl << "impossible" << endl << endl;
        else
        {
                sum = 0, f=0;
                sort(st.begin(), st.end(), myf);
                while(sum<stmps)
                {
                    sum=sum+st[f];
                    f=f+1;
                }
                cout << "Scenario #" << i << ":" << endl << f << endl << endl;
        }
        st.clear();
    }
    st.clear();
    return 0;
}
