#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#define BUGS 2000
using namespace std;
bool mark[BUGS+2];
int color[BUGS+2];

int main()
{
    ios::sync_with_stdio(false);
    int t,b, scnr=1, b1, b2, bugs, intrc, flag;
    cin >> t;
    for(int i=0; i<t; i=i+1)
    {
        vector<int> graph[BUGS+2];
        cin >> bugs >> intrc;
        memset(mark, false, sizeof(mark));
        memset(color, 0, sizeof(color));
        flag=0;
        for(int j=0; j<intrc; j++)
        {
            cin >> b1 >> b2;
            graph[b1].push_back(b2);
            graph[b2].push_back(b1);
        }
        queue<int> q;
        q.push(b1);
        mark[b1]=true;
        color[b1] = -1;
        while(!q.empty())
        {
            b = q.front();
            q.pop();
            for(int j=0; j<graph[b].size(); j=j+1)
            {
                if(!mark[graph[b][j]])
                {
                    mark[graph[b][j]] = true;
                    q.push(graph[b][j]);
                    color[graph[b][j]] = -1*color[b];
                }
                else
                {
                    if(color[graph[b][j]]==color[b])
                    {
                        flag=1;
                        break;
                    }
                }
            }
            if(flag==1)
                break;
        }
        if(!flag)
        {
            cout << "Scenario #" << scnr << ":" << endl;
            cout << "No suspicious bugs found!" << endl;
        }
        else
        {
            cout << "Scenario #" << scnr << ":" << endl;
            cout << "Suspicious bugs found!" << endl;
        }
        scnr=scnr+1;
    }
    return 0;
}
