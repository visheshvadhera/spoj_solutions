#include<iostream>
#define MAX 1000000
using namespace std;

long long int a[MAX] = {0};

long long int maxi(long long int a, long long int b)
{
    if(a>=b)
        return a;
    else
        return b;
}

unsigned long long int f(long long n)
{
    long long int x1,x2,x3,x;
    if(n==0)
        return 0;

    if(n<MAX)
    {
        if(a[n]==0)
        {
            x1 = f(n/2);
            x2 = f(n/3);
            x3 = f(n/4);
            x = x1+x2+x3;
            a[n] = maxi(n, x);
        }
        return a[n];
    }
    else
    {
        x1 = f(n/2);
        x2 = f(n/3);
        x3 = f(n/4);
        x = x1+x2+x3;
        return maxi(n, x);
    }

}

int main()
{
    ios::sync_with_stdio(false);
    long long coin_value;
    while(cin >> coin_value)
    {

        unsigned long long int b = f(coin_value);
        cout << b << endl;
    }
    return 0;
}
