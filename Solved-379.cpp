#include<iostream>
#include<string>
#include<sstream>
#include<vector>

using namespace std;

int perm[100001] = {0};
int inv_perm[100001] = {0};

int main()
{
    ios::sync_with_stdio(false);
    int i, digits, z;
    while(1)
    {
        cin >> digits;
        z=0;
        if(digits==0)
            break;
        else
        {
            for(i=1; i<=digits; i=i+1)
                cin >> perm[i];
            for(i=1; i<=digits; i=i+1)
                inv_perm[perm[i]] = i;
            for(i=1; i<=digits; i=i+1)
            {
                if(inv_perm[i]==perm[i])
                {
                    z=z+1;
                }
            }
            if(z==digits)
                cout << "ambiguous" << endl;
            else
                cout << "not ambiguous" << endl;
        }

    }
    return 0;
}
