#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
using namespace std;
#define MAX 500000

long long rs[MAX+8] = {0};
map<int, bool> mark;

int main()
{
    ios::sync_with_stdio(false);
    int n, i;
    mark[0] = true;
    for(i=1; i<=500000; i=i+1)
    {
        rs[i] = rs[i-1] - i;
        if(rs[i]<=0 || mark[rs[i]])
            rs[i] = rs[i-1]+i;
        mark[rs[i]]=true;
    }
    while(true)
    {
        cin >> n;
        if(n==-1)
            break;
        cout << rs[n] << endl;
    }
    return 0;
}
