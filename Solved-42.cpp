#include<iostream>
#include<string>
#include<queue>

using namespace std;

int power(int base, int p)
{
    if(p==0)
        return 1;
    if(p==1)
        return base;
    int t = power(base, p/2);
    return t*t*power(base, p%2);
}

int rev(int n)
{
    int rem, quo;
    quo = n;
    queue<int> q;
    rem = 0;

    if(n==0)
        return 0;
    else
    {
        while(quo!=0)
        {
            rem = quo%10;
            q.push(rem);
            quo = quo/10;
        }

        int size_of_queue = q.size();
        int reverse_no = 0;
        while(size_of_queue!=0)
        {
            reverse_no = reverse_no + (q.front())*power(10, size_of_queue-1);
            q.pop();
            size_of_queue = size_of_queue-1;
        }
        return reverse_no;
    }


}

int main()
{
    ios::sync_with_stdio(false);

    int cases;
    int n1, n2;
    cin >> cases;

    for(int i=1; i<=cases; i=i+1)
    {
        cin >> n1 >> n2;
        n1 = rev(n1);
        n2 = rev(n2);
        n1 = n1+n2;
        n1 = rev(n1);
        cout << n1 << endl;
    }

    return 0;
}
