#include<iostream>
#include<cmath>

using namespace std;

int factors(int x)
{
    int z = 0;
    int upper = sqrt(x);
    for(int j=1; j<=upper; j=j+1)
        if(x%j==0)
            z = z+1;

    return z;
}

int main()
{
    ios::sync_with_stdio(false);
    int input;
    cin >> input;
    int total = 1;
    for(int i=2;i<=input ;i=i+1)
        total=total+factors(i);
    cout << total << endl;

    return 0;
}
