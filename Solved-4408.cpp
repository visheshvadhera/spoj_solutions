#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<iomanip>
#include<cmath>
using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    double len;
    double pi = 4*atan(1.0);
    double area;
    while(true)
    {
        cin >> len;
        if(len==0)
            break;
        area = (len*len)/(2.0*pi);
        cout << fixed;
        cout << setprecision(2) << area << endl;

    }
    return 0;
}
