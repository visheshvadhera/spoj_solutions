#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
using namespace std;

int maxim(int a, int b) { return a>b?a:b;}

int main()
{
    ios::sync_with_stdio(false);
    int n, i, lo, len, m;
    int b, tot, sum1, sum2, hUpper, hDown, lUpper, lDown, j;
    while(true)
    {
        cin >> n;
        if(n==0)
            break;
        vector<int> up(n, 0);
        for(i=0; i<n; i=i+1){ cin >> b; up[i] = b;}
        cin >> m;
        vector<int> down(m, 0);
        for(i=0; i<m; i=i+1) {cin >> b; down[i] = b;}
        vector<int> upper_inter;
        vector<int> down_inter;
        for(i=0; i<n; i=i+1)
        {
            if(binary_search(down.begin(), down.end(), up[i]))
            {
                lo = lower_bound(down.begin(), down.end(), up[i]) - down.begin();
                upper_inter.push_back(i);
                down_inter.push_back(lo);
            }
        }
        len = down_inter.size();
        if(len==0)
        {
            sum1 = 0; sum2 = 0;
            for(i=0; i<n; i=i+1)
                sum1+=up[i];
            for(i=0; i<m; i=i+1)
                sum2+=down[i];
            cout << maxim(sum1, sum2) << endl;
        }
        else
        {
            tot = 0; sum1 = 0; sum2 = 0;
            hUpper = upper_inter[0]; lUpper = 0;
            hDown = down_inter[0]; lDown = 0;
            for(i=lUpper; i<=hUpper; i=i+1) sum1+=up[i];
            for(i=lDown; i<=hDown; i=i+1) sum2+=down[i];
            tot=maxim(sum1, sum2);
            for(j=1; j<len; j=j+1)
            {
                sum1 = 0; sum2 = 0;
                hUpper = upper_inter[j]; lUpper = upper_inter[j-1]+1;
                hDown = down_inter[j]; lDown = down_inter[j-1]+1;
                for(i=lUpper; i<=hUpper; i=i+1) sum1+=up[i];
                for(i=lDown; i<=hDown; i=i+1) sum2+=down[i];
                tot+=maxim(sum1, sum2);
            }
            sum1 = 0; sum2 = 0; j = len-1;
            hUpper = n-1; lUpper = upper_inter[j]+1;
            hDown = m-1; lDown = down_inter[j]+1;
            if(lUpper==n)
                sum1 = 0;
            else
                for(i=lUpper; i<=hUpper; i=i+1) sum1+=up[i];
            if(lDown==m)
                sum2 = 0;
            else
                for(i=lDown; i<=hDown; i=i+1) sum2+=down[i];
            tot+=maxim(sum1, sum2);
            cout << tot << endl;
        }
    }
    return 0;
}
