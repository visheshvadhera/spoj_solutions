#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
#define MAXN 101
using namespace std;


int main()
{
    ios::sync_with_stdio(false);
    int t, z, i, j, k;
    int d, e, f, a, b, c;
    long long cnt=0;
    cin >> t;
    vector<int> elements;
    for(i=0; i<t; i++)
    {
        cin >> z;
        elements.push_back(z);
    }
    vector<int> lhs;
    vector<int> rhs;
    for(i=0; i<t; i=i+1)
    {
        c = elements[i];
        f = elements[i];
        for(j=0; j<t; j=j+1)
        {
            b = elements[j];
            e = elements[j];
            for(k=0; k<t; k=k+1)
            {
                a = elements[k];
                d = elements[k];
                lhs.push_back(a*b+c);
                if(d==0)
                    continue;
                else
                    rhs.push_back(d*(e+f));
            }
        }
    }
    int len = lhs.size(), hi, lo;
    sort(rhs.begin(), rhs.end());
    sort(lhs.begin(), lhs.end());
    for(i=0; i<len; i=i+1)
    {
        lo = lower_bound(rhs.begin(), rhs.end(), lhs[i]) - rhs.begin();
        hi = upper_bound(rhs.begin(), rhs.end(), lhs[i]) - rhs.begin();
        cnt+=(hi-lo);
    }
    cout << cnt << endl;
    return 0;
}
