#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<deque>
#define min(a,b) a>b?b:a

using namespace std;

bool isEven(int a) {return a%2==0;}

char dir(int n, int m)
{
    int x, diff;
    if(n==1&&m==1)
        return 'R';
    else if(n%2==0 && m%2==0)
    {
        if(n==m)
            return 'L';
        else
        {
            x = min(n, m);
            diff = x-2;
            n = n-diff;
            m = m-diff;
            if(n==2)
                return 'L';
            else
                return 'U';
        }
    }
    else if(n%2==1 && m%2==1)
    {
        if(n==m)
            return 'R';
        else
        {
            x = min(n, m);
            diff = x-1;
            n = n-diff;
            m = m-diff;
            if(n==1) return 'R';
            else return 'D';
        }
    }
    else
    {
        x = min(n, m);
        if(isEven(x))
        {
            if(x==n)
                return 'L';
            else
                return 'U';
        }
        else
        {
            if(x==n)
                return 'R';
            else
                return 'D';
        }

    }

}

int main()
{
    ios::sync_with_stdio(false);
    int t, n, m;
    cin >> t;
    for(int i=0; i<t; i++)
    {
        cin >> n >> m;
        cout << dir(n, m) << endl;
    }

    return 0;
}
