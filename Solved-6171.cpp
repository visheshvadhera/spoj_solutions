#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
using namespace std;
int msg[2002];

void major(int n)
{
    int index, flag=0, target;
    for(int i=0; i<n; i=i+1)
    {
        cin >> index;
        if(flag==1)
            continue;
        msg[index+1000]+=1;
        if(msg[index+1000]>n/2)
        {
            flag=1;
            target = index;
        }
    }
    if(flag==1)
    {
        cout << "YES " << target << endl;
        cout << endl;
    }
    else
    {
        cout << "NO" << endl;
        cout << endl;
    }
}

int main()
{
    ios::sync_with_stdio(false);
    int t, n;
    cin >> t;
    for(int i=0; i<t; i=i+1)
    {
        memset(msg, 0, sizeof(msg));
        cin >> n;
        major(n);
    }


    return 0;
}
