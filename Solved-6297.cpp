#include<iostream>
#include<map>
#include<vector>
#include<cstdio>

#define sl(n) scanf("%lld", &n)

using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    int t;
    long long a, b, c;
    cin >> t;
    for(int i=0; i<t; i=i+1)
    {
        sl(a), sl(b), sl(c);
        cout << a*a-2*b <<  endl;
    }
    return 0;
}
