#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
#define REP(i,n) for(long long i=0; i<n; i=i+1)
#define MAXN 100000
using namespace std;
long long N, gap;
long long sa[MAXN], temp[MAXN], pos[MAXN];
bool sufCmp(long long i, long long j)
{
    if(pos[i]!=pos[j])
        return pos[i]<pos[j];
    i=i+gap;
    j=j+gap;
    if(i<N && j<N)
    {
        return pos[i]<pos[j];
    }
    else
        return i>j;
}

void buildSA(string s)
{
    N = s.length();
    REP(i, N) {sa[i] = i; pos[i] = s[i];}
    for(gap=1;;gap*=2)
    {
        sort(sa, sa+N, sufCmp);
        REP(i, N-1) {temp[i+1] = temp[i] + sufCmp(sa[i], sa[i+1]);}
        REP(i, N) {pos[sa[i]] = temp[i];}
        if(temp[N-1]==N-1)
            break;
    }
}

void makezero()
{
    memset(sa,0,sizeof(sa));
    memset(temp,0,sizeof(temp));
    memset(pos,0,sizeof(pos));
    N=0;
}

int main()
{
    ios::sync_with_stdio(false);
    makezero();
    string s;
    cin >> s;
    buildSA(s);
    for(int i=0; i<s.length(); i=i+1)
        cout << sa[i] << endl;


    return 0;
}
