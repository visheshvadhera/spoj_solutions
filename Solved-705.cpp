#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
#define REP(i,n) for(long long i=0; i<n; i=i+1)
#define MAXN 50000
using namespace std;
long long N, gap;
long long sa[MAXN], temp[MAXN], pos[MAXN];
long long  lcp[MAXN];

bool sufCmp(long long i, long long j)
{
    if(pos[i]!=pos[j])
        return pos[i]<pos[j];
    i=i+gap;
    j=j+gap;
    if(i<N && j<N)
    {
        return pos[i]<pos[j];
    }
    else
        return i>j;
}

void buildSA(string s)
{
    N = s.length();
    REP(i, N) {sa[i] = i; pos[i] = s[i];}
    for(gap=1;;gap*=2)
    {
        sort(sa, sa+N, sufCmp);
        REP(i, N-1) {temp[i+1] = temp[i] + sufCmp(sa[i], sa[i+1]);}
        REP(i, N) {pos[sa[i]] = temp[i];}
        if(temp[N-1]==N-1)
            break;
    }
}
long long mini(long long a, long long b){return a>b?b:a;}
void buildLCP(string s)
{
    lcp[0] = 0;
    long long cnt, l1, l2;
    for(long long k=1; k<N; k=k+1)
    {
        cnt = 0;
        l1 = N-sa[k-1];
        l2 = N-sa[k];
        for(long long i=0; i<mini(l1, l2); i=i+1)
        {
            if(s[sa[k]+i]!=s[sa[k-1]+i])
                break;
            cnt = cnt+1;
        }
        lcp[k] = cnt;
    }
}
long long sumLCP()
{
    long long sum=0;
    for(long long i=0; i<N; i=i+1)
    {
        sum += lcp[i];
    }
    return sum;
}
void makezero()
{
    memset(lcp,0,sizeof(lcp));
    memset(sa,0,sizeof(sa));
    memset(temp,0,sizeof(temp));
    memset(pos,0,sizeof(pos));
    N=0;
}

int main()
{
    ios::sync_with_stdio(false);
    long long t;
    cin >> t;
    for(long long i=0; i<t; i=i+1)
    {
        makezero();
        string s;
        cin >> s;
        buildSA(s);
        buildLCP(s);
        cout << ((N*(N+1))/2) - sumLCP() << endl;
    }

    return 0;
}
