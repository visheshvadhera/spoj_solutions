#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
#include<stack>

using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    long long t, q;
    int rem;
    stack<int> s;
    cin >> t;
    if(t==0)
        cout << 0;
    else
    {
    while(t!=0)
    {
        rem = t%(-2);
        q = t/(-2);
        if(rem==-1)
        {
            q = q+1;
            rem = 1;
            s.push(rem);
        }
        else
            s.push(rem);
        t = q;
    }
    while(!s.empty())
    {
        cout << s.top();
        s.pop();
    }
    cout << endl;
    }
    return 0;
}
