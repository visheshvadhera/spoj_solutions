#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<algorithm>
#include<stack>
#include<map>
using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    map<long long, bool> amap;
    amap[1] = true;
    int b=1, c=1, inp;
    while(b<=1000000000)
    {
        b=b+6*c;
        c=c+1;
        amap[b] = true;
    }
    while(true)
    {
        cin >> inp;
        if(inp==-1)
            break;

        if(amap[inp])
            cout << "Y" << endl;
        else
            cout << "N" << endl;
    }

    return 0;
}
