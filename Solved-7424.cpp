#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<algorithm>
#include<stack>
#include<cmath>

using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    double b, g;
    while(true)
    {
        cin >> g >> b;
        if(g==-1)
            break;
        if(g>=b)
            cout << static_cast<int>(ceil(g/(b+1.0))) << endl;
        else
            cout << static_cast<int>(ceil(b/(g+1.0))) << endl;
    }

    return 0;
}
