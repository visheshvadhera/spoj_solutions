#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
#define MAX 104
using namespace std;
int dp[MAX][MAX];
int fare[MAX][MAX];

int min2(int a, int b){ return a>b?b:a; }
int min3(int a, int b, int c)
{
    int min1;
    min1 = a>b?b:a;
    min1 = min1>c?c:min1;
    return min1;
}

int min_fare(int n, int m)
{
    int i, j, mini;
    for(i=1; i<=m; i++)
        dp[1][i] = fare[1][i];

    for(i=2; i<=n; i=i+1)
    {
        for(j=1; j<=m; j=j+1)
        {
            if(j==1)
            {
                dp[i][j] = min2(fare[i][j]+dp[i-1][j], fare[i][j]+dp[i-1][j+1]);
            }
            else if(j==m)
            {
                dp[i][j] = min2(fare[i][j]+dp[i-1][j], fare[i][j]+dp[i-1][j-1]);
            }
            else
            {
                dp[i][j] = min3(fare[i][j]+dp[i-1][j-1], fare[i][j]+dp[i-1][j], fare[i][j]+dp[i-1][j+1]);
            }
        }
    }
    mini = min2(dp[n][1], dp[n][2]);
    for(i=3; i<=m; i=i+1)
    {
        mini = min2(mini, dp[n][i]);
    }
    return mini;
}


int main()
{
    ios::sync_with_stdio(false);
    int n, m, i, j, mi = 0;
    cin >> n >> m;
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=m;j++)
        {
            cin >> fare[i][j];
        }
    }
    if(m==1)
    {
        for(i=1; i<=n; i++)
            mi += fare[i][1];
        cout << mi << endl;
    }
    else cout << min_fare(n, m) << endl;
    return 0;
}
