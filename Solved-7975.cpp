#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>

using namespace std;

long long cost[1000000][3];
int graph[1000000][3];

int min2(int a, int b) { return a>b?b:a; }

int min3(int a, int b, int c)
{
    int maxi;
    maxi = a>b?b:a;
    maxi = maxi>c?c:maxi;
    return maxi;
}

int min4(int a, int b, int c, int d)
{
    int mini1, mini2;
    mini1 = a>b?b:a;
    mini2 = c>d?d:c;
    return mini1>mini2?mini2:mini1;
}

long long dp(int rows)
{
    cost[0][0] = graph[0][0]; cost[0][1] = graph[0][1]; cost[0][2] = cost[0][1]+graph[0][2];
    cost[1][0] = cost[0][1]+graph[1][0];
    cost[1][1] = min3(cost[0][1]+graph[1][1], cost[0][2]+graph[1][1], cost[1][0]+graph[1][1]);
    cost[1][2] = min3(cost[0][1]+graph[1][2], cost[1][1]+graph[1][2], cost[0][2]+graph[1][2]);
    for(int i=2; i<rows;i=i+1)
    {
        cost[i][0] = min2(graph[i][0]+cost[i-1][0],graph[i][0]+cost[i-1][1]);
        cost[i][1] = min4(graph[i][1]+cost[i][0],graph[i][1]+cost[i-1][0],graph[i][1]+cost[i-1][1],graph[i][1]+cost[i-1][2]);
        cost[i][2] = min3(graph[i][2]+cost[i][1],graph[i][2]+cost[i-1][1],graph[i][2]+cost[i-1][2]);
    }
    return cost[rows-1][1];
}

int main()
{
    ios::sync_with_stdio(false);
    int rows,left,mid, right, i,cases=0;
    while(true)
    {
        cin >> rows;
        if(rows==0)
            break;
        cases = cases+1;
        for(i=0; i<rows; i=i+1)
        {
            cin >> graph[i][0] >> graph[i][1] >> graph[i][2];
        }
        cout << cases << ". " << dp(rows) << endl;

    }


    return 0;
}
