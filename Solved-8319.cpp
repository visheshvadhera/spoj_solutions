#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    int i,inp[10], sum[10];
    vector<int> diffP, diffN;
    for(i=0; i<10; i=i+1)
    {
        cin >> inp[i];
    }
    for(i=0; i<10; i=i+1)
    {
        sum[i] = inp[i];
    }
    for(i=1; i<10; i=i+1)
    {
        sum[i]+=sum[i-1];
    }
    for(i=0; i<10; i=i+1)
    {
        if(sum[i]<100)
            diffN.push_back(sum[i]-100);
        else
            diffP.push_back(sum[i]-100);
    }
    sort(diffP.begin(), diffP.end());
    sort(diffN.rbegin(), diffN.rend());
    if(diffP.empty()) cout << 100 + diffN[0] << endl;
    else if(diffN.empty()) cout << 100 + diffP[0] << endl;
    else if( diffP[0]<= abs(diffN[0])) cout << 100 + diffP[0] << endl;
    else cout << 100 + diffN[0] << endl;

    return 0;
}
