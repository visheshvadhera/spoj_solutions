#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
#include<iomanip>


using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    int t;
    double sa;
    long long v;
    cin >> t;
    for(int i=0; i<t; i=i+1)
    {
        cin >> v;
        sa = 6*sqrt(3)*pow((v*v)/4.0, 1.0/3.0);
        cout << fixed;
        cout << setprecision(10) << sa << endl;
    }

    return 0;
}
