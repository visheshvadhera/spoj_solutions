#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<algorithm>
#include<stack>
#include<iomanip>
using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    int t;
    int r;
    double s;
    cin >> t;
    for(int i=1; i<=t; i=i+1)
    {
        cin >> r;
        s = static_cast<double>((16.0*r*r+1.0)/4.0);
        cout << fixed;
        cout << "Case " << setprecision(2) << i << ":" << " " << s << endl;
    }

    return 0;
}
