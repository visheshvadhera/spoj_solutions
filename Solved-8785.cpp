#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>


using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    int n;
    int z, cuts;

    while(true)
    {
        cin >> n;
        if(n==0)
            break;
        z = 1; cuts=0;
        while(true)
        {
            z = z<<1;
            if(z>=n)
                break;
            cuts=cuts+1;
        }
        cout << cuts << endl;
    }

    return 0;
}
