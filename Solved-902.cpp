#include<iostream>

using namespace std;

int main()
{
    float input, sum;
    int i;
    do
    {
        cin >> input;
        if(input==0.00)
            continue;
        else
        {
            i=1;
            sum = 0.00;
            while(sum<=input)
            {
                sum = sum + 1/(i+1.0);
                i=i+1;
            }
            cout << i-1 << " card(s)" << endl;
        }
    }while(input!=0.00);

    return 0;
}
