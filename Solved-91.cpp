#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
using namespace std;

void yOrN(long long t)
{
    long long sqrT = sqrt(t);
    long long j=0;
    while(true)
    {
        if(j*j>t)
        {   cout << "No" << endl; break;}
        else if(sqrT*sqrT + j*j == t)
        {   cout << "Yes" << endl; break;}
        else if(sqrT*sqrT + j*j < t)
            j = j+1;
        else
            sqrT = sqrT-1;
    }
}

int main()
{
    ios::sync_with_stdio(false);
    int n;
    cin >> n;
    long long t;
    for(int i=0; i<n; i=i+1)
    {
        cin >> t;
        yOrN(t);
    }


    return 0;
}
