#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>

using namespace std;

int pow(int a, int n)
{
    if(n==0)
        return 1;
    if(n==1)
        return a;
    int t = pow(a, n/2);
    return t*t*pow(a, n%2);
}

int dcode(string s)
{
    string zero("S");
    string one(".");
    string two("..");
    string three("...");
    string four("....");
    string five("-");
    if(s.compare(zero)==0)
        return 0;
    else if(s.compare(one)==0)
        return 1;
    else if(s.compare(two)==0)
        return 2;
    else if(s.compare(three)==0)
        return 3;
    else if(s.compare(four)==0)
        return 4;
    else
        return 5;
}

int dig(string inp)
{
    int i=0;
    string q;
    stringstream ss;
    ss.clear();
    ss.str("");
    ss << inp;
    while(ss >> q)
    {
        i=i+dcode(q);
    }
    return i;
}

int ans(int n)
{
    int digit, j=n+1;
    int res=0;
    string inp;
    while(j>0)
    {
        getline(cin, inp);
        digit = dig(inp);
        if(j<=2)
            res+=digit*pow(20,j-1);
        else
            res+=digit*18*pow(20,j-2);
        j=j-1;
    }
    return res;
}

int main()
{
    ios::sync_with_stdio(false);
    int n, result;
    string blank;
    while(true)
    {
        cin >> n;
        if(n==0)
            break;
        result = ans(n);
        cout << result << endl;
    }



    return 0;
}
