#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<algorithm>
#include<stack>

int a[1000] = {0};
using namespace std;

void solver(int in)
{
    int i, refer = 1, b, z=0;
    stringstream o;
    stack<int> t;
    for(i=0; i<in; i=i+1)
    {
        if(a[i]!=refer)
        {
            if(!t.empty())
            {
                while(t.top()==refer)
                {
                    o << t.top() << " ";
                    t.pop();
                    refer = refer+1;
                    if(t.empty())
                        break;
                }

            }
            t.push(a[i]);
        }
        else
        {
            o << a[i] << " ";
            refer = refer+1;
        }
    }
    while(!t.empty())
    {
        o << t.top() << " ";
        t.pop();
    }
    for(i=1; i<=in; i=i+1)
    {
        o >> b;
        if(b==i)
            z=z+1;
    }
    if(z==in)
        cout << "yes" << endl;
    else
        cout << "no" << endl;
}

int main()
{
    ios::sync_with_stdio(false);
    int in, i, buff;
    while(true)
    {
        cin >> in;
        if(in==0)
            break;
        for(i=0; i<in; i=i+1)
        {
            cin >> buff;
            a[i] = buff;
        }
        solver(in);

    }

    return 0;
}
