#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
#define MAXN 100
using namespace std;
vector<int> friends[MAXN];
int main()
{
    ios::sync_with_stdio(false);
    int n, i, id, ff, j, z, cnt=0;
    map<int, bool> mark;
    map<int, bool>::iterator it;
    map<int, int> indexToID;
    cin >> n;
    for(i=0; i<n; i=i+1)
    {
        cin >> id >> ff;
        indexToID[i] = id;
        for(j=0; j<ff; j=j+1)
        {
            cin >> z;
            friends[i].push_back(z);
        }
    }
    for(i=0; i<n; i=i+1)
    {
        mark[indexToID[i]] = false;
    }
    for(i=0; i<n; i=i+1)
    {
        for(j=0; j<friends[i].size(); j=j+1)
        {
            it = mark.find(friends[i][j]);
            if(it==mark.end())
            {
                mark[friends[i][j]] = true;
                cnt=cnt+1;
            }
        }
    }
    cout << cnt << endl;

    return 0;
}
