#include<iostream>
#include<map>
#include<vector>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<string>
#include<cmath>
#define MAXN 300001
int val[MAXN];

using namespace std;
int maxim(int i, int j){return i>j?i:j; }

int main()
{
    ios::sync_with_stdio(false);
    int n, m;
    int left, right, sum, ans;
    cin >> n >> m;
    for(int i=1; i<=n; i=i+1)
    {
        cin >> val[i];
    }
    left=1; right = 1; sum = val[1];
    ans = 0;
    while(right<=n)
    {
        if(sum<=m)
        {
            ans = maxim(ans, sum);
            right=right+1;
            sum = sum + val[right];
        }
        else
        {
            left=left+1;
            sum = sum-val[left-1];
        }
    }
    cout << ans << endl;


    return 0;
}
