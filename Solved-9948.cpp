#include<iostream>
#include<string>
#include<sstream>
#include<vector>

using namespace std;

int main()
{
    ios::sync_with_stdio(false);
    unsigned long long int n;
    cin >> n;
    if(n%2==1)
        cout << "NIE" << endl;
    else
    {
        while(n!=1)
        {
                n=n/2;
                if(n%2==1 && n!=1)
                {
                    cout << "NIE" << endl;
                    break;
                }
        }
        if(n==1)
            cout << "TAK" << endl;
    }

    return 0;
}
